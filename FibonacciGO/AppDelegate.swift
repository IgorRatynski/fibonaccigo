//
//  AppDelegate.swift
//  FibonacciGO
//
//  Created by Igor Ratynski on 08.04.17.
//  Copyright © 2017 Igor Ratynski. All rights reserved.
//


import UIKit
import BigInt

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var presinter: Presenter?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let view = self.window?.rootViewController as! ViewController
        
        presinter = Presenter(with: view)
        view.presenter = presinter
        
        
        return true
    }
    
}
