//
//  ViewController.swift
//  FibonacciGO
//
//  Created by Igor Ratynski on 08.04.17.
//  Copyright © 2017 Igor Ratynski. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var presenter: Presenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:))))
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = UIRefreshControl()
            tableView.refreshControl?.addTarget(self, action: #selector(self.refreshOptions(sender:)), for: .valueChanged)
        } else {
            // тут должна быть крутая функция
        }
        
        tableView.addInfiniteScroll { (tableView) in
            self.presenter?.getNumbers()
            tableView.finishInfiniteScroll()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ViewController {
    //  MARK: Search Bar
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        NSLog("searchBarTextDidEndEditing")
        if searchBar.text! != "" {
            presenter?.search(number: UInt64(searchBar.text!)!)
        } else {
            presenter?.cancelSearch()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text! = ""
        presenter?.cancelSearch()
    }
    
    func checkAction(sender : UITapGestureRecognizer) {
        searchBar.resignFirstResponder()
    }
    
}

extension ViewController {
    //  MARK: Table View
    
    // UITableViewDelegate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == presenter!.numbers.count - 150 {
            
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36.0
    }
    
    // UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard presenter?.numbers.count != nil else {
            return 0;
        }
        
        return presenter!.numbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let number = presenter!.numbers[indexPath.row] as Number
        cell.set(id: String(number.ID!) , value: number.value!)
        return cell
    }
    
    @objc func refreshOptions(sender: UIRefreshControl) {
        
        presenter!.getNumbers()
    }
}

