//
//  FibonaccisFactory.swift
//  FibonacciGO
//
//  Created by Igor Ratynski on 08.04.17.
//  Copyright © 2017 Igor Ratynski. All rights reserved.
//

import UIKit
import BigInt


struct Number {
    let ID : UInt64?
    let value: String?
}

class FibonaccisFactory: NSObject {

    var allNumbers: [Number] = []
    
    public func go() {
        
        let queue = DispatchQueue.global(qos: .utility)
        queue.async {
                
            self.fibs().forEach {
                print($0)
                self.allNumbers.append($0)
            }
            
            
        }
    }
    
    func fibs() -> UnfoldSequence<Number, (UInt64, BigInt, BigInt)> {
        return sequence(state: (1, 1, 1),
                        next: { (pair: inout (UInt64, BigInt, BigInt)) -> Number? in
                            defer { pair = (pair.0 + 1 ,pair.2, pair.1 + pair.2) }
                            return Number.init(ID: pair.0, value: String(pair.2))
        })
    }
    
}


