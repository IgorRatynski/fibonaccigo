//
//  Presenter.swift
//  FibonacciGO
//
//  Created by Igor Ratynski on 22.04.17.
//  Copyright © 2017 Igor Ratynski. All rights reserved.
//

import UIKit

class Presenter: NSObject {
    
    var view: ViewController?

    var currentOperation = Operation()
    
    var fibonacci = FibonaccisFactory()
    
    var timer: Timer!
    
    var numbers = [Number]() {
            didSet {
                DispatchQueue.main.async {
                    self.view?.searchBar.placeholder = String.init(format: "Доступно чисел: %d", self.numbers.count)
                    self.view?.tableView.reloadData()
                    self.endRefreshing()
                }
            }
        }

    init(with orViewCntl: ViewController!) {
        super.init()
        fibonacci.go()
        view?.presenter = self
        view = orViewCntl
        
        setup()
    }
    
    private func setup() {
        
        numbers = fibonacci.allNumbers
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
    func runTimedCode() {
        
        if currentOperation.isExecuting == false {
            getNumbers()
        }
    }
    
    public func getNumbers() {
        
        self.numbers = self.fibonacci.allNumbers
    }
    
    public func search(number x: UInt64) {
        
        currentOperation.cancel()
        
        currentOperation = BlockOperation(block: {
            let queue = DispatchQueue.global(qos: .userInitiated)
            queue.async {
                
                if let offset = self.numbers.index(where: {$0.ID == x}) {
                    
                
                    //var rect = self.view?.tableView.rectForRow(at: IndexPath.init(row: offset, section: 0))
                    
                    //если обновлять из бэкграунда, то начинает сыпать ошибки и иногда падает
                    DispatchQueue.main.async {
                        self.view!.tableView.scrollToRow(at: IndexPath.init(row: offset, section: 0), at: .bottom, animated: true)
                        //не долетает
                        //self.view?.tableView.setContentOffset(rect!.origin, animated: true)
                    }
                }
                
                self.currentOperation.cancel()
            }
        })
        self.currentOperation.start()
        
        
        
    }
    
    public func cancelSearch() {
        currentOperation.cancel()
    }
    
    private func endRefreshing() {
        
        if #available(iOS 10.0, *) {
            self.view?.tableView.refreshControl?.endRefreshing()
        } else {
            
        }
    }
    
}
